package com.aspro;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * @author ASpro
 *
 */
public class Launcher extends Application {

    public static void main( String[] args ) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Button button = new Button();
        button.setText("Hello");
        button.setOnAction(actionEvent-> System.out.println("Hello Programmers!"));
        StackPane root = new StackPane();
        root.getChildren().add(button);

        Scene scene = new Scene(root, 500, 300);

        primaryStage.setTitle("Hello Programmers");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
